#include <iostream>
#include <fstream>
#include "BigNumber.h"
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <vector>

int main(int argc, char** argv)
{
	try
	{
		if (argc != 5)
		{
			throw std::runtime_error("Specify exactly 4 arguments.");
		}
		std::ifstream first_number_file(argv[1], std::ios_base::binary);
		if (first_number_file.fail())
		{
			throw std::runtime_error("Could not open first number file.");
		}
		first_number_file.seekg(0, first_number_file.end);
		size_t first_length = first_number_file.tellg();
		first_number_file.seekg(0);
		BigNumber first_number { std::string(first_length, 0) };
		first_number_file.read((char*)first_number.number.data(), first_length);
		if (first_number_file.fail())
		{
			throw std::runtime_error("Could not read first number file.");
		}
		first_number_file.close();
		std::ifstream second_number_file(argv[3], std::ios_base::binary);
		if (second_number_file.fail())
		{
			throw std::runtime_error("Could not open second number file.");
		}
		second_number_file.seekg(0, second_number_file.end);
		size_t second_length = second_number_file.tellg();
		second_number_file.seekg(0);
		BigNumber second_number { std::string(second_length, 0) };
		second_number_file.read((char*)second_number.number.data(), second_length);
		if (second_number_file.fail())
		{
			throw std::runtime_error("Could not read second number file.");
		}
		second_number_file.close();
		std::string specified_operator(argv[2]);
		if (specified_operator == "+")
		{
			auto result_length = std::max(first_length, second_length) + 1;
			BigNumber result { std::string(result_length, 0), result_length - 1 };
			BigNumber::add(first_number, second_number, result);
			std::ofstream output_number(argv[4], std::ios_base::binary);
			if (output_number.fail())
			{
				throw std::runtime_error("Could not open output number file.");
			}
			std::copy(result.number.begin() + result.start, result.number.end(), std::ostream_iterator<char>(output_number));
			if (output_number.fail())
			{
				throw std::runtime_error("Could not read output number file.");
			}
			return EXIT_SUCCESS;
		}
		else if (specified_operator == "-")
		{
			auto result_length = std::max(first_length, second_length);
			BigNumber result { std::string(result_length, 0), result_length - 1 };
			BigNumber::subtract(first_number, second_number, result);
			std::ofstream output_number(argv[4], std::ios_base::binary);
			if (output_number.fail())
			{
				throw std::runtime_error("Could not open output number file.");
			}
			std::copy(result.number.begin() + result.start, result.number.end(), std::ostream_iterator<char>(output_number));
			if (output_number.fail())
			{
				throw std::runtime_error("Could not read output number file.");
			}
			return EXIT_SUCCESS;
		}
		else if (specified_operator == "*")
		{
			auto result_length = first_length + second_length;
			BigNumber result { std::string(result_length, 0), result_length - 1 };
			BigNumber::multiply(first_number, second_number, result);
			std::ofstream output_number(argv[4], std::ios_base::binary);
			if (output_number.fail())
			{
				throw std::runtime_error("Could not open output number file.");
			}
			std::copy(result.number.begin() + result.start, result.number.end(), std::ostream_iterator<char>(output_number));
			if (output_number.fail())
			{
				throw std::runtime_error("Could not read output number file.");
			}
			return EXIT_SUCCESS;
		}
		else if (specified_operator == "<")
		{
			BigNumber result { std::string(1, 0) };
			auto compare_result = BigNumber::compare(first_number, second_number);
			if (compare_result < 0)
			{
				result.number[0] = '1';
			}
			else
			{
				result.number[0] = '0';
			}
			std::ofstream output_number(argv[4], std::ios_base::binary);
			if (output_number.fail())
			{
				throw std::runtime_error("Could not open output number file.");
			}
			std::copy(result.number.begin() + result.start, result.number.end(), std::ostream_iterator<char>(output_number));
			if (output_number.fail())
			{
				throw std::runtime_error("Could not read output number file.");
			}
			if (compare_result < 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else if (specified_operator == "=")
		{
			BigNumber result { std::string(1, 0) };
			auto compare_result = BigNumber::compare(first_number, second_number);
			if (compare_result == 0)
			{
				result.number[0] = '1';
			}
			else
			{
				result.number[0] = '0';
			}
			std::ofstream output_number(argv[4], std::ios_base::binary);
			if (output_number.fail())
			{
				throw std::runtime_error("Could not open output number file.");
			}
			std::copy(result.number.begin() + result.start, result.number.end(), std::ostream_iterator<char>(output_number));
			if (output_number.fail())
			{
				throw std::runtime_error("Could not read output number file.");
			}
			if (compare_result == 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else if (specified_operator == ">")
		{
			BigNumber result { std::string(1, 0) };
			auto compare_result = BigNumber::compare(first_number, second_number);
			if (compare_result > 0)
			{
				result.number[0] = '1';
			}
			else
			{
				result.number[0] = '0';
			}
			std::ofstream output_number(argv[4], std::ios_base::binary);
			if (output_number.fail())
			{
				throw std::runtime_error("Could not open output number file.");
			}
			std::copy(result.number.begin() + result.start, result.number.end(), std::ostream_iterator<char>(output_number));
			if (output_number.fail())
			{
				throw std::runtime_error("Could not read output number file.");
			}
			if (compare_result > 0)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			throw std::runtime_error("Specified operator is not supported.");
		}
	}
	catch (std::exception& err)
	{
		std::cerr << err.what();
		return EXIT_FAILURE;
	}
}
