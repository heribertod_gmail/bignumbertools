#include <iostream>
#include <fstream>
#include "BigNumber.h"
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <vector>

int main(int argc, char** argv)
{
	try
	{
		if (argc != 3)
		{
			throw std::runtime_error("Specify exactly 2 arguments.");
		}
		std::ifstream input_binary(argv[1], std::ios_base::binary);
		if (input_binary.fail())
		{
			throw std::runtime_error("Could not open input binary file.");
		}
		input_binary.seekg(0, input_binary.end);
		size_t input_length = input_binary.tellg();
		input_binary.seekg(0);
		std::vector<unsigned char> binary(input_length);
		input_binary.read((char*)binary.data(), input_length);
		if (input_binary.fail())
		{
			throw std::runtime_error("Could not read input binary file.");
		}
		input_binary.close();
		BigNumber number { std::string(4 * binary.size(), 0), 4 * binary.size() - 1 };
		number.number[number.number.length() - 1] = '0';
		BigNumber::to_number(binary.data(), binary.size(), number);
		std::ofstream output_number(argv[2], std::ios_base::binary);
		if (output_number.fail())
		{
			throw std::runtime_error("Could not open output number file.");
		}
		std::copy(number.number.begin() + number.start, number.number.end(), std::ostream_iterator<char>(output_number));
		if (output_number.fail())
		{
			throw std::runtime_error("Could not read output number file.");
		}
		return EXIT_SUCCESS;
	}
	catch (std::exception& err)
	{
		std::cerr << err.what();
		return EXIT_FAILURE;
	}
}
