#include <iostream>
#include <fstream>
#include "BigNumber.h"
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <vector>

int main(int argc, char** argv)
{
	try
	{
		if (argc != 3)
		{
			throw std::runtime_error("Specify exactly 2 arguments.");
		}
		std::ifstream input_number(argv[1], std::ios_base::binary);
		if (input_number.fail())
		{
			throw std::runtime_error("Could not open input number file.");
		}
		input_number.seekg(0, input_number.end);
		size_t input_length = input_number.tellg();
		input_number.seekg(0);
		BigNumber number { std::string(8 * input_length, 0), 7 * input_length };
		input_number.read((char*)number.number.data() + 7 * input_length, input_length);
		if (input_number.fail())
		{
			throw std::runtime_error("Could not read input number file.");
		}
		input_number.close();
		std::vector<unsigned char> binary(input_length);
		auto binary_start = BigNumber::to_binary(number, binary.data(), binary.size());
		std::ofstream output_binary(argv[2], std::ios_base::binary);
		if (output_binary.fail())
		{
			throw std::runtime_error("Could not open output binary file.");
		}
		output_binary.write((char*)binary.data() + binary_start, binary.size() - binary_start);
		if (output_binary.fail())
		{
			throw std::runtime_error("Could not read output binary file.");
		}
		return EXIT_SUCCESS;
	}
	catch (std::exception& err)
	{
		std::cerr << err.what();
		return EXIT_FAILURE;
	}
}
