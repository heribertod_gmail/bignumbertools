#pragma once

#include <string>

struct BigNumber
{
	std::string number;
	std::size_t start;
	
	static void add(BigNumber& first, BigNumber& second, BigNumber& result);
	
	static void subtract(BigNumber& first, BigNumber& second, BigNumber& result);
	
	static void multiply(BigNumber& first, BigNumber& second, BigNumber& result);

	static int compare(BigNumber& first, BigNumber& second);

	static void to_number(unsigned char* binary, std::size_t length, BigNumber& number);

	static std::size_t to_binary(BigNumber& number, unsigned char* binary, std::size_t length);
};

