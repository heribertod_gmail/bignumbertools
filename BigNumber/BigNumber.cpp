#include "BigNumber.h"
#include <stdexcept>

void BigNumber::add(BigNumber& first, BigNumber& second, BigNumber& result)
{
	if (first.start >= first.number.length() || second.start >= second.number.length())
	{
		throw std::invalid_argument("Cannot add empty numbers.");
	}
	if (result.number.length() == 0)
	{
		throw std::invalid_argument("Result number is empty.");
	}
	auto index_of_first = first.number.length() - 1;
	auto index_of_second = second.number.length() - 1;
	auto index_of_result = result.number.length() - 1;
	auto first_ended = false;
	auto second_ended = false;
	auto carry = 0;
	while (!first_ended || !second_ended || carry > 0)
	{
		int first_digit;
		if (first_ended)
		{
			first_digit = 0;
		}
		else
		{
			first_digit = first.number[index_of_first] - '0';
		}
		int second_digit;
		if (second_ended)
		{
			second_digit = 0;
		}
		else
		{
			second_digit = second.number[index_of_second] - '0';
		}
		auto result_digit = carry + first_digit + second_digit;
		if (result_digit >= 10)
		{
			carry = 1;
			result_digit -= 10;
		}
		else
		{
			carry = 0;
		}
		result.number[index_of_result] = result_digit + '0';
		if (!first_ended)
		{
			if (index_of_first > first.start)
			{
				index_of_first--;
			}
			else
			{
				first_ended = true;
			}
		}
		if (!second_ended)
		{
			if (index_of_second > second.start)
			{
				index_of_second--;
			}
			else
			{
				second_ended = true;
			}
		}
		if (!first_ended || !second_ended || carry > 0)
		{
			if (index_of_result > 0)
			{
				index_of_result--;
			}
			else
			{
				throw std::invalid_argument("Result is too small.");
			}
		}
	}
	result.start = index_of_result;
}

void BigNumber::subtract(BigNumber& first, BigNumber& second, BigNumber& result)
{
	if (first.start >= first.number.length() || second.start >= second.number.length())
	{
		throw std::invalid_argument("Cannot subtract empty numbers.");
	}
	if (result.number.length() == 0)
	{
		throw std::invalid_argument("Result number is empty.");
	}
	auto index_of_first = first.number.length() - 1;
	auto index_of_second = second.number.length() - 1;
	auto index_of_result = result.number.length() - 1;
	auto first_ended = false;
	auto second_ended = false;
	auto borrow = 0;
	auto zero_count = 0;
	auto zero_location = index_of_result;
	while (!first_ended || !second_ended || borrow > 0)
	{
		int first_digit;
		if (first_ended)
		{
			first_digit = 0;
		}
		else
		{
			first_digit = first.number[index_of_first] - '0';
		}
		int second_digit;
		if (second_ended)
		{
			second_digit = 0;
		}
		else
		{
			second_digit = second.number[index_of_second] - '0';
		}
		second_digit += borrow;
		if (first_digit < second_digit)
		{
			if (index_of_first == first.start)
			{
				throw std::invalid_argument("Result would be negative.");
			}
			else
			{
				first_digit += 10;
				borrow = 1;
			}
		}
		else
		{
			borrow = 0;
		}
		auto result_digit = first_digit - second_digit;
		if (result_digit == 0)
		{
			zero_count++;
		}
		else
		{
			zero_count = 0;
		}
		if (zero_count == 1)
		{
			zero_location = index_of_result;
		}
		result.number[index_of_result] = result_digit + '0';
		if (!first_ended)
		{
			if (index_of_first > first.start)
			{
				index_of_first--;
			}
			else
			{
				first_ended = true;
			}
		}
		if (!second_ended)
		{
			if (index_of_second > second.start)
			{
				index_of_second--;
			}
			else
			{
				second_ended = true;
			}
		}
		if (!first_ended || !second_ended || borrow > 0)
		{
			if (index_of_result > 0)
			{
				index_of_result--;
			}
			else
			{
				throw std::invalid_argument("Result is too small.");
			}
		}
	}
	if (zero_count > 0)
	{
		if (zero_count == result.number.length())
		{
			result.start = zero_location;
		}
		else
		{
			result.start = zero_location + 1;
		}
	}
	else
	{
		result.start = index_of_result;
	}
}

void BigNumber::multiply(BigNumber& first, BigNumber& second, BigNumber& result)
{
	if (first.start >= first.number.length() || second.start >= second.number.length())
	{
		throw std::invalid_argument("Cannot multiply empty numbers.");
	}
	if (result.number.length() == 0)
	{
		throw std::invalid_argument("Result number is empty.");
	}
	auto result_coverage_set = false;
	auto result_coverage_begin = result.number.length();
	auto result_coverage_end = (size_t)0;
	auto index_of_second = second.number.length() - 1;
	auto second_ended = false;
	auto last_in_result = result.number.length() - 1;
	auto remaining_in_second = second.number.length() - second.start;
	while (remaining_in_second > 0)
	{
		int second_digit;
		if (second_ended)
		{
			second_digit = 0;
		}
		else
		{
			second_digit = second.number[index_of_second] - '0';
		}
		auto index_of_first = first.number.length() - 1;
		auto index_of_result = last_in_result;
		auto first_ended = false;
		auto carry_of_multiplication = 0;
		auto carry_of_addition = 0;
		while (!first_ended || carry_of_multiplication > 0 || carry_of_addition > 0)
		{
			int first_digit;
			if (first_ended)
			{
				first_digit = 0;
			}
			else
			{
				first_digit = first.number[index_of_first] - '0';
			}
			auto result_digit = carry_of_multiplication + first_digit * second_digit;
			carry_of_multiplication = result_digit / 10;
			result_digit = result_digit % 10;
			if (result_coverage_set)
			{
				if (index_of_result >= result_coverage_begin && index_of_result <= result_coverage_end)
				{
					auto accumulated = result.number[index_of_result] - '0';
					accumulated = carry_of_addition + accumulated + result_digit;
					if (accumulated >= 10)
					{
						carry_of_addition = 1;
						accumulated -= 10;
					}
					else
					{
						carry_of_addition = 0;
					}
					result.number[index_of_result] = accumulated + '0';
				}
				else
				{
					result_digit = carry_of_addition + result_digit;
					if (result_digit >= 10)
					{
						carry_of_addition = 1;
						result_digit -= 10;
					}
					else
					{
						carry_of_addition = 0;
					}
					result.number[index_of_result] = result_digit + '0';
				}
			}
			else
			{
				result.number[index_of_result] = result_digit + '0';
			}
			if (result_coverage_begin > index_of_result)
			{
				result_coverage_begin = index_of_result;
			}
			if (result_coverage_end < index_of_result)
			{
				result_coverage_end = index_of_result;
			}
			if (!first_ended)
			{
				if (index_of_first > first.start)
				{
					index_of_first--;
				}
				else
				{
					first_ended = true;
				}
			}
			if (!first_ended || carry_of_multiplication > 0 || carry_of_addition > 0)
			{
				if (index_of_result > 0)
				{
					index_of_result--;
				}
				else
				{
					throw std::invalid_argument("Result is too small.");
				}
			}
		}
		if (!second_ended)
		{
			if (index_of_second > second.start)
			{
				index_of_second--;
			}
			else
			{
				second_ended = true;
			}
		}
		remaining_in_second--;
		if (remaining_in_second > 0)
		{
			result_coverage_set = true;
			last_in_result--;
		}
	}
	result.start = result_coverage_begin;
}

int BigNumber::compare(BigNumber& first, BigNumber& second)
{
	if (first.start >= first.number.length() || second.start >= second.number.length())
	{
		throw std::invalid_argument("Cannot compare empty numbers.");
	}
	auto size_of_first = first.number.length() - first.start;
	auto size_of_second = second.number.length() - second.start;
	if (size_of_first > size_of_second)
	{
		return 1;
	}
	else if (size_of_first < size_of_second)
	{
		return -1;
	}
	else
	{
		auto index_of_first = first.start;
		auto index_of_second = second.start;
		while (size_of_first > 0)
		{
			auto first_digit = first.number[index_of_first];
			auto second_digit = second.number[index_of_second];
			if (first_digit > second_digit)
			{
				return 1;
			}
			else if (first_digit < second_digit)
			{
				return -1;
			}
			index_of_first++;
			index_of_second++;
			size_of_first--;
		}
		return 0;
	}
}

void BigNumber::to_number(unsigned char* binary, std::size_t length, BigNumber& number)
{
	if (length == 0)
	{
		throw std::invalid_argument("Cannot convert empty binary.");
	}
	BigNumber power_of_two { number.number, number.start };
	power_of_two.number[power_of_two.number.length() - 1] = '1';
	BigNumber two { "2" };
	auto index = length - 1;
	auto binary_ended = false;
	while (!binary_ended)
	{
		auto character = binary[index];
		for (auto i = 0; i < 8; i++)
		{
			if (character % 2 == 1)
			{
				BigNumber::add(number, power_of_two, number);
			}
			BigNumber::multiply(power_of_two, two, power_of_two);
			character /= 2;
		}
		if (index > 0)
		{
			index--;
		}
		else
		{
			binary_ended = true;
		}
	}
}

std::size_t BigNumber::to_binary(BigNumber& number, unsigned char* binary, std::size_t length)
{
	if (number.start >= number.number.length())
	{
		throw std::invalid_argument("Cannot convert empty number.");
	}
	BigNumber zero { "0" };
	BigNumber five { "5" };
	auto index = length - 1;
	auto mask = (unsigned char)1;
	auto character = (unsigned char)0;
	do
	{
		BigNumber::multiply(number, five, number);
		auto last_digit = number.number[number.number.length() - 1];
		number.number.resize(number.number.length() - 1);
		if (last_digit != '0')
		{
			character |= mask;
		}
		if (mask == 128)
		{
			binary[index] = character;
			character = 0;
			mask = 1;
			if (index > 0)
			{
				index--;
			}
			else
			{
				throw std::invalid_argument("Result binary is too small");
			}
		}
		else
		{
			mask *= 2;
		}
	} 
	while (number.start < number.number.length() && BigNumber::compare(number, zero) > 0);
	if (mask == 1)
	{
		return index + 1;
	}
	else
	{
		binary[index] = character;
		return index;
	}
}
